﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows;


namespace AI_Project_I
{
    public partial class Form1 : Form
    {
        public double cohesionWeight;
        public double alignmentWeight;
        public double separationWeight;
        public double obstacleWeight;
        public double dangerWeight;
        public PaintEventArgs painter;
        public List<Bird> birds;
        public List<Bird> predators;
        public List<Obstacle> obstacles;
        public Random r;
        public Form1()
        {
            InitializeComponent();
           

            trackBarAlignment.Value = 5;
            trackBarCohesion.Value = 5;
            trackBarSeparation.Value = 5;

            cohesionWeight = 5 + Convert.ToDouble(trackBarCohesion.Value) / 20;
            alignmentWeight = 5 + Convert.ToDouble(trackBarAlignment.Value) / 20;
            separationWeight = 5 + Convert.ToDouble(trackBarSeparation.Value) / 20;
            obstacleWeight = 4; //was 5.8
            dangerWeight = 10;

            birds = new List<Bird>();
            predators = new List<Bird>();
            obstacles = new List<Obstacle>();
            r = new Random();
            for (int i = 0; i < 50; i++)
                birds.Add(new Bird(r));
            /*this.SetStyle(
                ControlStyles.AllPaintingInWmPaint |
                ControlStyles.UserPaint |
                ControlStyles.DoubleBuffer,
                true);*/
            System.Drawing.Graphics graphics = this.CreateGraphics();
            System.Drawing.Rectangle rectangle = new System.Drawing.Rectangle(0, 0, 500, 500);  //drawing area
            painter = new PaintEventArgs(graphics, rectangle);

        }


        public void DrawEllipseFloat(PaintEventArgs e, Bird bird, Pen currentPen)
        {

                       

            int x = Convert.ToInt32(bird.position.X);
            int y = Convert.ToInt32(bird.position.Y);
            int width = 10;
            int height = 10;


            e.Graphics.DrawEllipse(currentPen, x - width / 2, y - width / 2, width, height);  //draw elipse with radius 5 at the position of bird

            Vector normalized = new Vector(bird.velocity.X, bird.velocity.Y);
            normalized.Normalize();
            normalized *= 5;    //so it matches radius of elipse
            System.Drawing.Point ending = new System.Drawing.Point(Convert.ToInt32(normalized.X + x), Convert.ToInt32(normalized.Y + y));   //translate at position of bird
            //System.Drawing.Point ending = new System.Drawing.Point(x + Convert.ToInt32(bird.velocity.X / bird.velocity.Length*5), y + Convert.ToInt32(bird.velocity.Y / bird.velocity.Length*5)); //alternative

            System.Drawing.Point starting = new System.Drawing.Point(x, y);


            e.Graphics.DrawLine(currentPen, starting, ending);    //draw line that represents velocity

        }
        protected override void OnPaint(PaintEventArgs e)
        {
            //System.Threading.Thread.Sleep(15);
            Pen blackPen = new Pen(Color.Black, 1); //new pen
            foreach (Bird bird in birds)
            {
                if (bird.position.X <= 0)
                    bird.position.X = 500;
                else
                    if (bird.position.X >= 500)
                        bird.position.X = 0;
                if (bird.position.Y <= 0)
                    bird.position.Y = 500;
                else
                    if (bird.position.Y >= 500)
                        bird.position.Y = 0;
                DrawEllipseFloat(painter, bird, blackPen);
            }
            Pen redPen = new Pen(Color.Red, 1); //new pen
            foreach (Bird predator in predators)
            {
                if (predator.position.X <= 0)
                    predator.position.X = 500;
                else
                    if (predator.position.X >= 500)
                        predator.position.X = 0;
                if (predator.position.Y <= 0)
                    predator.position.Y = 500;
                else
                    if (predator.position.Y >= 500)
                        predator.position.Y = 0;
                DrawEllipseFloat(painter, predator, redPen);
            }
            Pen greenPen = new Pen(Color.Green, 2);
            foreach (Obstacle obstacle in obstacles)
                e.Graphics.DrawEllipse(greenPen, Convert.ToInt32(obstacle.position.X), Convert.ToInt32(obstacle.position.Y), Convert.ToInt32(obstacle.radius), Convert.ToInt32(obstacle.radius));
            updateBoid(40);
            Invalidate();
           // Update();
           // Refresh();
        }

        /*public double distance(Bird a, Bird b)
        {
            double xdif = b.position.X - a.position.X;
            double ydif = b.position.Y - a.position.Y;
            return Math.Sqrt(xdif * xdif + ydif * ydif);
        }
        */
        public double distance(double x1, double y1, double x2, double y2)
        {
            double xdif = x2 - x1;
            double ydif = y2 - y1;
            return Math.Sqrt(xdif * xdif + ydif * ydif);
        }
        public List<Bird> findNeighbours(Bird current, int radius)
        {
            List<Bird> neighbours = new List<Bird>();
            foreach (Bird otherbird in birds)
                if (current != otherbird && distance(current.position.X,current.position.Y, otherbird.position.X,otherbird.position.Y) < radius)
                    neighbours.Add(otherbird);
            return neighbours;
        }
        public void updateBoid(int radius)
        {
            foreach (Bird bird in birds)
            {
                List<Bird> neighbours = findNeighbours(bird,radius);
                Vector separation = separationWeight * calculateSeparationForce(neighbours, bird);
                Vector alignment = alignmentWeight * calculateAlignmentForce(neighbours);
                Vector cohesion = cohesionWeight * calculateCohesionForce(neighbours, bird);
                Vector obstacle = obstacleWeight * calculateObstacleAvoidance(bird);
                Vector danger = dangerWeight * calculateDanger(bird,radius);
                bird.velocity = bird.velocity + separation + alignment + cohesion + obstacle + danger;
                bird.velocity.Normalize();
                bird.position = bird.position + bird.velocity;

            }
            foreach (Bird predator in predators)
            {
                double minDistance = 1000;
                double temp;
                Bird minBird = new Bird(r); //have to initialize
                foreach (Bird bird in birds)
                {
                    temp=distance(predator.position.X,predator.position.Y,bird.position.X,bird.position.Y);
                    if (temp<minDistance)
                    {
                        minDistance = temp;
                        minBird = bird;
                    }
                }
                Vector chase = predator.position - minBird.position;
                chase.Negate();
                chase.Normalize();
                predator.velocity = predator.velocity + chase;
                predator.velocity.Normalize();
                predator.position = predator.position + predator.velocity;
            }
        }

        private Vector calculateDanger(Bird bird, int radius)
        {
            Vector v = new Vector();
            int predatorCount = 0;
            foreach (Bird predator in predators)
            {
                if (distance(predator.position.X,predator.position.Y,bird.position.X,bird.position.Y)<radius)
                {
                    v += bird.position - predator.position;
                    predatorCount++;
                }
            }
            if (predatorCount==0)
                return v;
            else
            {
                v = v / predatorCount;
                v.Normalize();
                return v;
            }            
        }

        private Vector calculateCohesionForce(List<Bird> neighbours, Bird current)
        {
            Vector v = new Vector();
            if (neighbours.Count == 0)
                return v;
            else
            {
                foreach (Bird bird in neighbours)
                {
                    v += bird.position;
                }
                v = v / neighbours.Count;
                v = v - current.position;
                v.Normalize();
                return v;
            }
        }

        private Vector calculateAlignmentForce(List<Bird> neighbours)
        {
            Vector v = new Vector();
            if (neighbours.Count == 0)
                return v;
            else
            {
                foreach (Bird bird in neighbours)
                {
                    v += bird.velocity;
                }
                v = v / neighbours.Count;
                v.Normalize();
                return v;
            }
        }

        private Vector calculateSeparationForce(List<Bird> neighbours, Bird current)
        {
            Vector v = new Vector();
            if (neighbours.Count == 0)
                return v;
            else
            {
                foreach (Bird bird in neighbours)
                {
                    v += bird.position - current.position;
                }
                v = v / neighbours.Count;
                v.Negate();
                v.Normalize();
                return v;
            }
        }
        private Vector calculateObstacleAvoidance(Bird bird)
        {
            Vector v = new Vector();
            List<Obstacle> neighbours = new List<Obstacle>();
            foreach (Obstacle obstacle in obstacles)
                if (distance(bird.position.X, bird.position.Y, obstacle.position.X, obstacle.position.Y) < obstacle.radius + 5) //size of bird
                    neighbours.Add(obstacle);
            if (neighbours.Count == 0)
                return v;
            else
            {
                foreach (Obstacle obstacle in neighbours)
                {
                    v.X += obstacle.position.X - bird.position.X;   //had to do like this since obstacle.position is a point
                    v.Y += obstacle.position.Y - bird.position.Y;
                }
                v.X = v.X / neighbours.Count;
                v.Y = v.Y / neighbours.Count;
                v.Negate();
                v.Normalize();
                return v;
            }
        }/*
        private Vector calculateObstacleAvoidance(Bird bird)
        {
            Vector v = new Vector();
            Obstacle closestobstacle = new Obstacle(r); //have to initialize
            double closestdistance = 1000;
            bool check=false;
            foreach (Obstacle obstacle in obstacles)
            {
                double tempdistance = distance(bird.position.X, bird.position.Y, obstacle.position.X, obstacle.position.Y);
                if (tempdistance < 40 && tempdistance<closestdistance)
                {
                    closestdistance = tempdistance;
                    closestobstacle = obstacle;
                    check = true;
                }
                    
            }
            if (!check)
                return v;
            else
            {
                v.X = closestobstacle.position.X - bird.position.X;   //had to do like this since obstacle.position is a point
                v.Y = closestobstacle.position.Y - bird.position.Y;
                v.Negate();
                Vector v1 = new Vector(-v.X, v.Y);
                v1.Normalize();
                v1 = v1 * (closestobstacle.radius);
                v.Normalize();
                v = v + v1;
                v.Normalize();
                return v;
            }
        }
        */
        private void trackBarSeparation_Scroll(object sender, EventArgs e)
        {
            separationWeight = 5 + Convert.ToDouble(trackBarSeparation.Value) / 20;
        }

        private void trackBarAlignment_Scroll(object sender, EventArgs e)
        {
            alignmentWeight = 5 + Convert.ToDouble(trackBarAlignment.Value) / 20;
        }

        private void trackBarCohesion_Scroll(object sender, EventArgs e)
        {
            cohesionWeight = 5 + Convert.ToDouble(trackBarCohesion.Value) / 20;
        }

        private void buttonAddObstacle_Click(object sender, EventArgs e)
        {
            obstacles.Add(new Obstacle(r));
        }
        protected override CreateParams CreateParams    //reduce flickering
        {
            get
            {
                CreateParams cp = base.CreateParams;
                cp.ExStyle |= 0x02000000;  // Turn on WS_EX_COMPOSITED
                return cp;
            }
        }
        
        private void buttonPredator_Click(object sender, EventArgs e)
        {
            predators.Add(new Bird(r));
        }

        private void buttonRemoveObstacle_Click(object sender, EventArgs e)
        {
            if (obstacles.Count > 0)
                obstacles.RemoveAt(obstacles.Count - 1);
        }

        private void buttonRemovePredator_Click(object sender, EventArgs e)
        {
            if (predators.Count > 0)
                predators.RemoveAt(predators.Count - 1);
        }        
    }
}
