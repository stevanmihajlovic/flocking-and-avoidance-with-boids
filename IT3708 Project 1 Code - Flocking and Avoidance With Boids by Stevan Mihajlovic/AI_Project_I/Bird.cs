﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
namespace AI_Project_I
{
    public class Bird
    {
        public Vector position;
        public Vector velocity;
        public Bird(Random r)
        {
            position.X = r.Next(500);   //so its somewhere in the area
            position.Y = r.Next(500);
            velocity.X = r.NextDouble()*10 - 5;
            velocity.Y = r.NextDouble()*10 - 5;
        }
        
        

    }
}
