﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace AI_Project_I
{
    public class Obstacle
    {
        public Point position;
        public double radius;
        public Obstacle(Random r)
        {
            position.X = r.Next(480);   //because of radius
            position.Y = r.Next(480);
            radius = r.Next(15)+5;
        }
    }
}
