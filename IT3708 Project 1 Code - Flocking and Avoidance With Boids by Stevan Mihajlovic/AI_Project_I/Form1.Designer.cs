﻿namespace AI_Project_I
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.trackBarSeparation = new System.Windows.Forms.TrackBar();
            this.trackBarAlignment = new System.Windows.Forms.TrackBar();
            this.trackBarCohesion = new System.Windows.Forms.TrackBar();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.buttonAddObstacle = new System.Windows.Forms.Button();
            this.buttonPredator = new System.Windows.Forms.Button();
            this.buttonRemoveObstacle = new System.Windows.Forms.Button();
            this.buttonRemovePredator = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarSeparation)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarAlignment)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarCohesion)).BeginInit();
            this.SuspendLayout();
            // 
            // trackBarSeparation
            // 
            this.trackBarSeparation.Location = new System.Drawing.Point(522, 40);
            this.trackBarSeparation.Minimum = 1;
            this.trackBarSeparation.Name = "trackBarSeparation";
            this.trackBarSeparation.Size = new System.Drawing.Size(104, 45);
            this.trackBarSeparation.TabIndex = 0;
            this.trackBarSeparation.Value = 1;
            this.trackBarSeparation.Scroll += new System.EventHandler(this.trackBarSeparation_Scroll);
            // 
            // trackBarAlignment
            // 
            this.trackBarAlignment.Location = new System.Drawing.Point(522, 109);
            this.trackBarAlignment.Minimum = 1;
            this.trackBarAlignment.Name = "trackBarAlignment";
            this.trackBarAlignment.Size = new System.Drawing.Size(104, 45);
            this.trackBarAlignment.TabIndex = 1;
            this.trackBarAlignment.Value = 1;
            this.trackBarAlignment.Scroll += new System.EventHandler(this.trackBarAlignment_Scroll);
            // 
            // trackBarCohesion
            // 
            this.trackBarCohesion.Location = new System.Drawing.Point(522, 184);
            this.trackBarCohesion.Minimum = 1;
            this.trackBarCohesion.Name = "trackBarCohesion";
            this.trackBarCohesion.Size = new System.Drawing.Size(104, 45);
            this.trackBarCohesion.TabIndex = 2;
            this.trackBarCohesion.Value = 1;
            this.trackBarCohesion.Scroll += new System.EventHandler(this.trackBarCohesion_Scroll);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(522, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(61, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Separation:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(522, 88);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(56, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Alignment:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(522, 168);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(54, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "Cohesion:";
            // 
            // buttonAddObstacle
            // 
            this.buttonAddObstacle.Location = new System.Drawing.Point(525, 236);
            this.buttonAddObstacle.Name = "buttonAddObstacle";
            this.buttonAddObstacle.Size = new System.Drawing.Size(101, 23);
            this.buttonAddObstacle.TabIndex = 6;
            this.buttonAddObstacle.Text = "Add Obstacle";
            this.buttonAddObstacle.UseVisualStyleBackColor = true;
            this.buttonAddObstacle.Click += new System.EventHandler(this.buttonAddObstacle_Click);
            // 
            // buttonPredator
            // 
            this.buttonPredator.Location = new System.Drawing.Point(525, 295);
            this.buttonPredator.Name = "buttonPredator";
            this.buttonPredator.Size = new System.Drawing.Size(101, 23);
            this.buttonPredator.TabIndex = 7;
            this.buttonPredator.Text = "Add Predator";
            this.buttonPredator.UseVisualStyleBackColor = true;
            this.buttonPredator.Click += new System.EventHandler(this.buttonPredator_Click);
            // 
            // buttonRemoveObstacle
            // 
            this.buttonRemoveObstacle.Location = new System.Drawing.Point(525, 266);
            this.buttonRemoveObstacle.Name = "buttonRemoveObstacle";
            this.buttonRemoveObstacle.Size = new System.Drawing.Size(101, 23);
            this.buttonRemoveObstacle.TabIndex = 8;
            this.buttonRemoveObstacle.Text = "Remove Obstacle";
            this.buttonRemoveObstacle.UseVisualStyleBackColor = true;
            this.buttonRemoveObstacle.Click += new System.EventHandler(this.buttonRemoveObstacle_Click);
            // 
            // buttonRemovePredator
            // 
            this.buttonRemovePredator.Location = new System.Drawing.Point(525, 324);
            this.buttonRemovePredator.Name = "buttonRemovePredator";
            this.buttonRemovePredator.Size = new System.Drawing.Size(101, 23);
            this.buttonRemovePredator.TabIndex = 9;
            this.buttonRemovePredator.Text = "Remove Predator";
            this.buttonRemovePredator.UseVisualStyleBackColor = true;
            this.buttonRemovePredator.Click += new System.EventHandler(this.buttonRemovePredator_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(639, 461);
            this.Controls.Add(this.buttonRemovePredator);
            this.Controls.Add(this.buttonRemoveObstacle);
            this.Controls.Add(this.buttonPredator);
            this.Controls.Add(this.buttonAddObstacle);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.trackBarCohesion);
            this.Controls.Add(this.trackBarAlignment);
            this.Controls.Add(this.trackBarSeparation);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.trackBarSeparation)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarAlignment)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarCohesion)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public System.Windows.Forms.TrackBar trackBarSeparation;
        public System.Windows.Forms.TrackBar trackBarAlignment;
        public System.Windows.Forms.TrackBar trackBarCohesion;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button buttonAddObstacle;
        private System.Windows.Forms.Button buttonPredator;
        private System.Windows.Forms.Button buttonRemoveObstacle;
        private System.Windows.Forms.Button buttonRemovePredator;
    }
}

